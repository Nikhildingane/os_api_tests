package com.krishagni.openspecimen.tests.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.lang3.StringUtils;

import com.krishagni.openspecimen.tests.dd.Config;

public class DbUtil {
	private static final String SSH = "ssh -i %s %s@%s";

	private static final String MYSQL = "mysql -u%s -p%s -h%s -P%s";

	private static final String CREATE_DB = "create database %s";

	private static final String DROP_DB = "drop database %s";

	public static int createDatabase(Config.Db db) {
		return executeCmd(db, String.format(CREATE_DB, db.getDatabase()));
	}

	public static int dropDatabase(Config.Db db) {
		return executeCmd(db, String.format(DROP_DB, db.getDatabase()));
	}

	public static int importData(Config.Db db, File input) {
		InputStream in = null;
		OutputStream out = null;

		try {
			Process process = launchMySql(db, true);
			in = new FileInputStream(input);
			out = process.getOutputStream();

			int numRead;
			byte[] buffer = new byte[16 * 1024];
			while ((numRead = in.read(buffer, 0, buffer.length)) > 0) {
				out.write(buffer, 0, numRead);
				out.flush();
			}

			out.close();
			return process.waitFor();
		} catch (Exception e) {
			throw new RuntimeException("Error importing the data", e);
		} finally {
			if (in != null) {
				try { in.close(); } catch (Exception e) { }
			}

			if (out != null) {
				try { out.close(); } catch (Exception e) { }
			}
		}
	}

	private static int executeCmd(Config.Db db, String cmd) {
		try {
			Process process = launchMySql(db, false);
			sendCmd(process, cmd);
			return process.waitFor();
		} catch (Exception e) {
			throw new RuntimeException("Error executing command: " + cmd, e);
		}
	}

	private static Process launchMySql(Config.Db db, boolean connectToDb) {
		try {
			String ssh = "";
			if (StringUtils.isNotBlank(db.getSshUsername()) && StringUtils.isNotBlank(db.getSshPrivateKey())) {
				ssh = String.format(SSH, db.getSshPrivateKey(), db.getSshUsername(), db.getHost());
			}

			String dbHost = db.getHost();
			if (StringUtils.isNotBlank(ssh)) {
				dbHost = "localhost";
			}

			String mysql = String.format(MYSQL, db.getUsername(), db.getPassword(), dbHost, db.getPort());
			String cmd = ssh + " " + mysql;
			if (connectToDb) {
				cmd = cmd + " " + db.getDatabase();
			}

			System.err.println(cmd);
			return Runtime.getRuntime().exec(cmd.trim());
		} catch (Exception e) {
			throw new RuntimeException("Error connecting to the database", e);
		}
	}

	private static void sendCmd(Process process, String cmd) {
		try {
			OutputStream out = process.getOutputStream();
			out.write(cmd.getBytes());
			out.flush();
			out.close();
		} catch (Exception e) {
			throw new RuntimeException("Error executing the MySQL command: " + cmd);
		}
	}
}
