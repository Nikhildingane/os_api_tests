package com.krishagni.openspecimen.tests.dd;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.testng.Assert;
import org.testng.ITest;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.krishagni.openspecimen.tests.util.DbUtil;
import com.krishagni.openspecimen.tests.util.ExpressionUtil;
import com.opencsv.CSVReader;

public class DataDrivenTester implements ITest {
	private Config config;

	private String testName;

	private String testFile;

	private String dataDump;

	private CSVReader csvReader;

	private String[] fieldNames;

	@Parameters({"dataDump", "testFile", "configFile"})
	@BeforeClass
	public void setupSuite(String dataDump, String testFile, String configFile) {
		this.dataDump = dataDump;
		this.testFile = testFile;
		this.config = Config.parse(configFile);
		setupDb();
	}

	@BeforeMethod
	public void setupTest(Method method, Object[] input) {
		if (input != null && input.length > 0 && input[0] instanceof Map) {
			testName = (String) ((Map) input[0]).get("_desc");
		} else {
			testName = "Unknown";
		}
	}

	@Override
	public String getTestName() {
		return testName;
	}

	@DataProvider(name="inputCsvIterator")
	public Iterator<Object> inputRow()
	throws Exception {
		InputStream in = getClass().getResourceAsStream(testFile);
		csvReader = new CSVReader(new InputStreamReader(in));

		fieldNames = csvReader.readNext();

		return new Iterator<Object>() {

			private String[] last;

			@Override
			public boolean hasNext() {
				if (last == null) {
					last = nextLine();
				}

				return last != null;
			}

			@Override
			public Map<String, String> next() {
				String[] next;
				if (last != null) {
					next = last;
				} else {
					next = nextLine();
				}

				last = null;
				if (next == null) {
					return null;
				}

				Map<String, String> result = new HashMap<>();
				for (int i = 0; i < fieldNames.length; ++i) {
					if (i < next.length) {
						result.put(fieldNames[i], next[i]);
					}
				}

				return result;
			}

			private String[] nextLine() {
				try {
					return csvReader.readNext();
				} catch (Exception e) {
					e.printStackTrace();
					return null;
				}
			}
		};
	}

	@Test(dataProvider = "inputCsvIterator")
	public void runTest(Object input)
	throws Exception {
		TestRequest request = TestRequest.parse((Map<String, String>) input);
		TestResponse response = request.exchange(config);

		Assert.assertEquals(response.getStatus(), request.getExpectedStatusCode());
		if (request.getExpectedErrorCodes() != null) {
			Assert.assertNotNull(response.getBody(), "Response body is null");
			for (String expectedCode : request.getExpectedErrorCodes()) {
				if (Arrays.stream(response.getErrorCodes()).noneMatch(code -> code.equalsIgnoreCase(expectedCode))) {
					Assert.fail(expectedCode + " not found in the response");
				}
			}

			Assert.assertEqualsNoOrder(response.getErrorCodes(), request.getExpectedErrorCodes());
		}

		if (request.getAssertions() != null) {
			Map<String, Object> ctxt = Collections.singletonMap("response", response.getBodyAsJavaObject());
			for (String assertion : request.getAssertions()) {
				Assert.assertTrue(ExpressionUtil.getInstance().evaluate(assertion, ctxt));
			}
		}
	}

	@AfterClass
	public void teardownSuite()
	throws Exception {
		testFile = null;
		fieldNames = null;
		if (csvReader != null) {
			csvReader.close();
		}
		//teardownDb();
	}

	private void setupDb() {
		try {
			File file = new File(getClass().getResource(dataDump).toURI());
			DbUtil.dropDatabase(config.getDb());
			Assert.assertEquals(DbUtil.createDatabase(config.getDb()), 0, "Error creating database");
			Assert.assertEquals(DbUtil.importData(config.getDb(), file), 0, "Error importing data");
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error importing the database", e);
		}
	}

	private void teardownDb() {
		try {
			DbUtil.dropDatabase(config.getDb());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Error deleting the database", e);
		}
	}
}
