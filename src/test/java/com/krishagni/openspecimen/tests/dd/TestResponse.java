package com.krishagni.openspecimen.tests.dd;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestResponse {
	private int status;

	private String body;

	public TestResponse(int status, String body) {
		this.status = status;
		this.body = body;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Object getBodyAsJavaObject() {
		try {
			return new ObjectMapper().readValue(body, Object.class);
		} catch (Exception e) {
			throw new RuntimeException("Error parsing the response body to Java object", e);
		}
	}

	public String[] getErrorCodes() {
		if (status / 400 != 1 && status / 500 != 1) {
			return new String[0];
		}

		Object pojo = getBodyAsJavaObject();
		if (!(pojo instanceof List)) {
			return new String[0];
		}

		return ((List<Map<String, String>>) pojo).stream()
			.map(err -> err != null ? err.get("code") : null)
			.filter(Objects::nonNull)
			.toArray(String[]::new);
	}
}
