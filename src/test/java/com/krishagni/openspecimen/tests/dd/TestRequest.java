package com.krishagni.openspecimen.tests.dd;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

public class TestRequest {
	private static final Pattern ARRAY_IDX = Pattern.compile("^(\\w+)\\[(\\d+)\\]$");

	enum Method {
		HEAD,
		GET,
		POST,
		PUT,
		PATCH,
		DELETE
	}

	String desc;

	Method method;

	String apiUrl;

	String user;

	int expectedStatusCode;

	String[] expectedErrorCodes;

	String[] assertions;

	Map<String, Object> payload;

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Method getMethod() {
		return method;
	}

	public void setMethod(Method method) {
		this.method = method;
	}

	public String getApiUrl() {
		return apiUrl;
	}

	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int getExpectedStatusCode() {
		return expectedStatusCode;
	}

	public void setExpectedStatusCode(int expectedStatusCode) {
		this.expectedStatusCode = expectedStatusCode;
	}

	public String[] getExpectedErrorCodes() {
		return expectedErrorCodes;
	}

	public void setExpectedErrorCodes(String[] expectedErrorCodes) {
		this.expectedErrorCodes = expectedErrorCodes;
	}

	public String[] getAssertions() {
		return assertions;
	}

	public void setAssertions(String[] assertions) {
		this.assertions = assertions;
	}

	public Map<String, Object> getPayload() {
		return payload;
	}

	public void setPayload(Map<String, Object> payload) {
		this.payload = payload;
	}

	public TestResponse exchange(Config cfg) {
		HttpHeaders headers = new HttpHeaders();
		headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
		headers.set("Accept-Encoding", "gzip,deflate");
		headers.set("Accept", "application/json, text/plain, */*");

		Config.User cfgUser = cfg.getUser(user);
		if (cfgUser != null) {
			headers.setBasicAuth(cfgUser.getUsername(), cfgUser.getPassword());
		}

		Map<String, Object> body = null;
		if (getMethod() == Method.POST || getMethod() == Method.PUT) {
			body = getPayload();
		}

		RestTemplate client = new RestTemplate();
		try {
			ResponseEntity<String> resp = client.exchange(
				cfg.getUrl(getApiUrl()),
				HttpMethod.valueOf(getMethod().name()),
				new HttpEntity<>(body, headers),
				String.class);
			return new TestResponse(resp.getStatusCodeValue(), resp.getBody());
		} catch (HttpStatusCodeException httpEx) {
			return new TestResponse(httpEx.getRawStatusCode(), httpEx.getResponseBodyAsString());
		}
	}

	public static TestRequest parse(Map<String, String> input) {
		Map<String, Object> result = new HashMap<>();
		for (Map.Entry<String, String> kv : input.entrySet()) {
			String[] parts = kv.getKey().split("\\.");
			if (StringUtils.isBlank(kv.getValue())) {
				continue;
			}

			Map<String, Object> r = result;
			for (int i = 0; i <= parts.length - 1; ++i) {
				Matcher arrayIdxMatcher = ARRAY_IDX.matcher(parts[i]);
				if (arrayIdxMatcher.matches()) {
					String name = arrayIdxMatcher.group(1);
					int idx     = Integer.parseInt(arrayIdxMatcher.group(2));
					List<Object> array = (List<Object>) r.computeIfAbsent(name, (k) -> new ArrayList<>());
					if (idx > array.size() - 1) {
						for (int j = array.size() - 1; j < idx; ++j) {
							array.add(null);
						}
					}

					if (i == parts.length - 1) {
						array.set(idx, isSetToNull(kv.getValue()) ? null : kv.getValue());
					} else {
						r = (Map<String, Object>) array.get(idx);
						if (r == null) {
							r = new HashMap<>();
							array.set(idx, r);
						}
					}
				} else {
					if (i == parts.length - 1) {
						r.put(parts[i],  isSetToNull(kv.getValue()) ? null : kv.getValue());
					} else {
						r = (Map<String, Object>) r.computeIfAbsent(parts[i], (k) -> new HashMap<String, Object>());
					}
				}
			}
		}

		return convertMap(result);
	}

	private static boolean isSetToNull(String value) {
		return "##set_to_blank##".equals(value);
	}

	private static TestRequest convertMap(Map<String, Object> input) {
		Map<String, Object> testData = new HashMap<>();
		Map<String, Object> payload = new HashMap<>();
		testData.put("payload", payload);

		for (Map.Entry<String, Object> kv : input.entrySet()) {
			if (kv.getKey().startsWith("_")) {
				testData.put(kv.getKey().substring(1), kv.getValue());
			} else {
				payload.put(kv.getKey(), kv.getValue());
			}
		}

		return new ObjectMapper().convertValue(testData, TestRequest.class);
	}
}
