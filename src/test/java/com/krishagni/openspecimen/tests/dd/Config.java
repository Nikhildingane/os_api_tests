package com.krishagni.openspecimen.tests.dd;

import java.io.InputStream;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Config {
	private String serverUrl;

	private Map<String, User> users;

	private Db db;

	public String getServerUrl() {
		return serverUrl;
	}

	public void setServerUrl(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public String getApiRoot() {
		return getServerUrl() + "rest/ng/";
	}

	public Map<String, User> getUsers() {
		return users;
	}

	public void setUsers(Map<String, User> users) {
		this.users = users;
	}

	public Db getDb() {
		return db;
	}

	public void setDb(Db db) {
		this.db = db;
	}

	public User getUser(String role) {
		if (StringUtils.isBlank(role)) {
			return null;
		}

		return users.get(role);
	}

	public String getUrl(String endpoint) {
		if (StringUtils.isBlank(getServerUrl())) {
			return endpoint;
		}

		if (endpoint.startsWith("/")) {
			endpoint = endpoint.substring(1);
		}

		return getApiRoot() + endpoint;
	}

	public static Config parse(String configFilePath) {
		InputStream in = null;
		try {
			in = Config.class.getResourceAsStream(configFilePath);
			Config cfg = new ObjectMapper()
				.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
				.readValue(in, Config.class);

			if (StringUtils.isNotBlank(cfg.getServerUrl()) && !cfg.getServerUrl().endsWith("/")) {
				cfg.setServerUrl(cfg.getServerUrl() + "/");
			}

			return cfg;
		} catch (Exception e) {
			throw new RuntimeException("Error reading configuration data", e);
		} finally {
			if (in != null) {
				try { in.close(); } catch (Exception ce) { }
			}
		}
	}

	public static class User {
		String username;

		String password;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}
	}

	public static class Db {
		String host;

		String port;

		String username;

		String password;

		String database;

		String sshUsername;

		String sshPrivateKey;

		public String getHost() {
			return host;
		}

		public void setHost(String host) {
			this.host = host;
		}

		public String getPort() {
			return port;
		}

		public void setPort(String port) {
			this.port = port;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getDatabase() {
			return database;
		}

		public void setDatabase(String database) {
			this.database = database;
		}

		public String getSshUsername() {
			return sshUsername;
		}

		public void setSshUsername(String sshUsername) {
			this.sshUsername = sshUsername;
		}

		public String getSshPrivateKey() {
			return sshPrivateKey;
		}

		public void setSshPrivateKey(String sshPrivateKey) {
			this.sshPrivateKey = sshPrivateKey;
		}
	}
}
