package com.krishagni.openspecimen.tests.report;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.util.FileCopyUtils;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

public class RunReport implements IReporter {

	@Override
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDir) {
		List<SuiteStat> stats = new ArrayList<>();
		for (ISuite suite : suites) {
			for (ISuiteResult result : suite.getResults().values()) {
				ITestContext testContext = result.getTestContext();

				SuiteStat stat = new SuiteStat();
				stat.setName(suite.getName());
				stat.setPassed(testContext.getPassedTests().size());
				stat.setFailed(testContext.getFailedTests().size());
				stat.setSkipped(testContext.getSkippedTests().size());

				for (ITestResult testResult : testContext.getFailedTests().getAllResults()) {
					stat.addFailedTest(testResult.getName());
				}

				stats.add(stat);
			}
		}

		generateReport(stats, outputDir);
	}

	private void generateReport(List<SuiteStat> stats, String outputDir) {
		StringBuilder summary = new StringBuilder();
		StringBuilder failedTests = new StringBuilder();
		for (SuiteStat stat : stats) {
			summary.append("<tr>")
				.append("<td style=\"padding:8px;border-bottom:1px solid #ddd;\">")
					.append(stat.getName())
				.append("</td>")
				.append("<td style=\"padding:8px;border-bottom:1px solid #ddd;\">")
					.append(stat.getPassed())
				.append("</td>")
				.append("<td style=\"padding:8px;border-bottom:1px solid #ddd;\">")
					.append(stat.getFailed())
				.append("</td>")
				.append("<td style=\"padding:8px;border-bottom:1px solid #ddd;\">")
					.append(stat.getSkipped())
				.append("</td>")
			.append("</tr>");

			if (!stat.getFailedTests().isEmpty()) {
				failedTests.append("<tr>")
					.append("<td style=\"padding:8px;border-bottom:1px solid #ddd;\">")
						.append(stat.getName())
					.append("</td>")
					.append("<td style=\"padding:8px;border-bottom:1px solid #ddd;\">")
						.append("<ul>")
							.append(stat.getFailedTests().stream()
								.map(t -> "<li>" + t + "</li>")
								.collect(Collectors.joining()))
						.append("</ul>")
					.append("</td>")
				.append("</tr>");
			}
		}

		if (failedTests.length() == 0) {
			failedTests.append("<tr>")
				.append("<td style=\"padding:8px;border-bottom:1px solid #ddd;\">")
					.append("None")
				.append("</td>")
			.append("</tr>");
		}

		try {
			File file = new File(outputDir, "run-report.html");
			FileCopyUtils.copy(String.format(HTML_TMPL, summary.toString(), failedTests.toString()).getBytes(), file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static final String HTML_TMPL =
		"<html>" +
		"  <head>" +
		"    <title>OpenSpecimen Tests Execution Report</title>" +
		"  </head>" +
		"  <body>" +
		"    <div>" +
		"      Dear all, <br><br>" +
		"      Given below is the summary of the automated tests execution: <br>" +
		"    </div>" +
		"    <h4><u>Summary</u></h4>" +
		"    <table style=\"padding:8px;border-collapse: collapse;\">" +
		"      <thead>" +
		"        <tr>" +
		"          <th style=\"padding:8px;border-bottom:1px solid #ddd;\">Suite</th>" +
		"          <th style=\"padding:8px;border-bottom:1px solid #ddd;\">Passed</th>" +
		"          <th style=\"padding:8px;border-bottom:1px solid #ddd;\">Failed</th>" +
		"          <th style=\"padding:8px;border-bottom:1px solid #ddd;\">Skipped</th>" +
		"        </tr>" +
		"      </thead>" +
		"      <tbody>" +
		"        %s" +
		"      </tbody>" +
		"    </table>" +
		"    <br>" +
		"    <h4><u>Failed Tests</u></h4>" +
		"    <table style=\"padding:8px;border-collapse: collapse;\">" +
		"      <thead>" +
		"        <tr>" +
		"          <th style=\"padding:8px;border-bottom:1px solid #ddd;\">Suite</th>" +
		"          <th style=\"padding:8px;border-bottom:1px solid #ddd;\">Tests</th>" +
		"        </tr>" +
		"      </thead>" +
		"      <tbody>" +
		"        %s" +
		"      </tbody>" +
		"    </table>"	+
		"  </body>" +
		"</html>";

	private class SuiteStat {
		private String name;

		private int passed;

		private int failed;

		private int skipped;

		private List<String> failedTests = new ArrayList<>();

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public int getPassed() {
			return passed;
		}

		public void setPassed(int passed) {
			this.passed = passed;
		}

		public int getFailed() {
			return failed;
		}

		public void setFailed(int failed) {
			this.failed = failed;
		}

		public int getSkipped() {
			return skipped;
		}

		public void setSkipped(int skipped) {
			this.skipped = skipped;
		}

		public List<String> getFailedTests() {
			return failedTests;
		}

		public void setFailedTests(List<String> failedTests) {
			this.failedTests = failedTests;
		}

		public void addFailedTest(String name) {
			System.err.println(name);
			failedTests.add(name);
		}
	}

}